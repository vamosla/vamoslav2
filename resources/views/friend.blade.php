@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <!-- busca -->
                        <form action="/friend" method="POST">
                            {{ csrf_field() }}
                            
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">Buscar</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group col-sm-12">
                                    <input class="form-control" name="search">
                                    <div class="input-group-append">
                                        <button class="btn btn-info BuscaFriendList"><span class="fa fa-search"></span></button>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <!-- / busca -->
                        
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead class="thead-dark">
                            <tbody>
                            @if( $friends )
                                @foreach($friends as $friend)
                                    <tr>
                                        <td class="col-sm-4">
                                            <div>{{ $friend->name }}</div>
                                        </td>
                                        <td class="col-sm-4">
                                            <div>{{ $friend->email }}</div>
                                        </td>

                                        <!-- Delete Button -->
                                        <td class="col-sm-4">
                                            <button class="btn btn-danger"><span class="fa fa-remove"></span> Excluir</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="col-sm-12" colspan="2">Nenhum Amigo. :(</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    @component('modal.criar')
        
        @slot('title') Adicionar Amigo @endslot
        
        @slot('footer')
            <button type="button" class="btn btn-primary">Adicionar</button>
        @endslot

        <div class="form-group">
            <div class="input-group">
                <div class="col-sm-12">
                    <input class="form-control" name="Buscar">
                </div>
                <div class="btn btn-primary"><span class="fa fa-search"></span> Buscar</div>
            </div>
        </div>

    @endcomponent

@endsection


