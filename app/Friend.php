<?php

namespace App;


use app\User;
use Illuminate\Database\Eloquent\Model;
#use Illuminate\Database\Eloquent\HasOne;

class Friend extends Model
{

	# table
	protected $table = 'friend';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','user_friend_id', 'ex',
    ];

    // CONNECTION TO USER
	    public function User()
	    {
	        return $this->hasOne(User::class,'id','user_friend_id');
	    }
	    /*public function userFriend()
	    {
	        return $this->hasOne('App\User','user_friend_id');
	    }*/
	// CONNECTION TO USER
}
